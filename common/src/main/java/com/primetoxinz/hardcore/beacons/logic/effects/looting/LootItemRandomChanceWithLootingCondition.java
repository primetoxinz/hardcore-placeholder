package com.primetoxinz.hardcore.beacons.logic.effects.looting;

import com.google.common.collect.ImmutableSet;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.primetoxinz.hardcore.beacons.HardcoreBeacons;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.parameters.LootContextParam;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemConditionType;

import java.util.Set;

public class LootItemRandomChanceWithLootingCondition implements LootItemCondition {
    final float percent;
    final float lootingMultiplier;

    LootItemRandomChanceWithLootingCondition(float f, float g) {
        this.percent = f;
        this.lootingMultiplier = g;
    }

    public LootItemConditionType getType() {
        return HardcoreBeacons.LOOTING_EFFECT_CONDITION.get();
    }

    public Set<LootContextParam<?>> getReferencedContextParams() {
        return ImmutableSet.of(LootContextParams.KILLER_ENTITY);
    }

    public boolean test(LootContext lootContext) {
        Entity entity = (Entity)lootContext.getParamOrNull(LootContextParams.KILLER_ENTITY);
        int i = 0;
        if (entity instanceof LivingEntity) {
            i = EnchantmentHelper.getMobLooting((LivingEntity)entity);
        }

        return lootContext.getRandom().nextFloat() < this.percent + (float)i * this.lootingMultiplier;
    }

    public static LootItemCondition.Builder randomChanceAndLootingBoost(float f, float g) {
        return () -> {
            return new LootItemRandomChanceWithLootingCondition(f, g);
        };
    }

    public static class Serializer implements net.minecraft.world.level.storage.loot.Serializer<LootItemRandomChanceWithLootingCondition> {
        public Serializer() {
        }

        public void serialize(JsonObject jsonObject, LootItemRandomChanceWithLootingCondition lootItemRandomChanceWithLootingCondition, JsonSerializationContext jsonSerializationContext) {
            jsonObject.addProperty("chance", lootItemRandomChanceWithLootingCondition.percent);
            jsonObject.addProperty("looting_multiplier", lootItemRandomChanceWithLootingCondition.lootingMultiplier);
        }

        public LootItemRandomChanceWithLootingCondition deserialize(JsonObject jsonObject, JsonDeserializationContext jsonDeserializationContext) {
            return new LootItemRandomChanceWithLootingCondition(GsonHelper.getAsFloat(jsonObject, "chance"), GsonHelper.getAsFloat(jsonObject, "looting_multiplier"));
        }
    }
}

