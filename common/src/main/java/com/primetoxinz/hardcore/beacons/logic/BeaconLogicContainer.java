package com.primetoxinz.hardcore.beacons.logic;

import com.google.common.collect.Lists;
import com.primetoxinz.hardcore.beacons.game.features.beacon.BeaconBlockEntity;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.state.BlockState;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class BeaconLogicContainer {

    public static Function<BeaconEffect, Predicate<LivingEntity>> isPlayerNotWearing(EquipmentSlot slot, Predicate<ItemStack> isWearingTag) {
        return beaconEffect -> entityLivingBase -> {
            if (entityLivingBase instanceof Player) {
                if (entityLivingBase.hasItemInSlot(slot)) {
                    return isWearingTag.test(entityLivingBase.getItemBySlot(slot));
                }
            }
            return true;
        };
    }


    private static void updateBeacon(Level level, BlockPos pos, BeaconLogicContainer.StateContainer beaconState) {
        BlockState stateBelow = level.getBlockState(pos.below());


         BeaconEffects.beaconEffects.values().stream()
                .filter(beaconEffect ->
                        stateBelow.is(beaconEffect.getStructureTag())
                )
                .findFirst().ifPresent(beaconEffect -> {
                    beaconState.setEffect(beaconEffect);
                });



        if (beaconState.getEffect() != null) {
            int levels = calculateLevels(level, pos, beaconState);
            beaconState.setComplete(levels > 0);
            beaconState.setLevel(levels);
            updateColorSegments(level, pos, beaconState);
        } else {
            beaconState.beamSegments.clear();
        }

    }

    private static int calculateLevels(Level world, BlockPos pos, BeaconLogicContainer.StateContainer beaconState) {
        final int MAX_LEVELS = 4;
        BlockState stateAtPosition;
        int radius = 0;
        for (radius = 1; radius <= MAX_LEVELS; radius++) {
            for (int x = -radius; x <= radius; x++) {
                for (int z = -radius; z <= radius; z++) {
                    stateAtPosition = world.getBlockState(pos.offset(x, -radius, z));
                    if (!stateAtPosition.is(beaconState.effect.getStructureTag())) {
                        return radius - 1;
                    }
                }
            }
        }
        return radius - 1;
    }


    public static void onRemoved(StateContainer stateContainer, Level level, BlockPos pos) {
        SoundEvent soundEvent = SoundEvents.BEACON_DEACTIVATE;
        level.playSound((Player) null, pos, soundEvent, SoundSource.BLOCKS, 1.0F, 1.0F);
        if(stateContainer != null) {
            var effect = stateContainer.getEffect();
            if(effect != null) {
                effect.onBreak(level, pos, stateContainer);
            }
        }
    }


    public static void tick(Level level, BlockPos blockPos, BlockState blockState, BeaconBlockEntity beaconBlockEntity) {
        if (level.getGameTime() % 80L == 0L) {
            BeaconLogicContainer.StateContainer beaconState = beaconBlockEntity.getStateContainer();
            var previousComplete = beaconState.isComplete;

            updateBeacon(level, blockPos, beaconState);
            if (beaconState.isComplete()) {
                level.playSound((Player) null, blockPos, SoundEvents.BEACON_AMBIENT, SoundSource.BLOCKS, 1.0F, 1.0F);
                beaconState.getEffect().apply(level, blockPos, beaconState);
            }

            if (!level.isClientSide && beaconState.isComplete() != previousComplete) {
                SoundEvent soundEvent = beaconState.isComplete() ? SoundEvents.BEACON_ACTIVATE : SoundEvents.BEACON_DEACTIVATE;
                level.playSound((Player) null, blockPos, soundEvent, SoundSource.BLOCKS, 1.0F, 1.0F);
                beaconState.getEffect().onCreate(level, blockPos, beaconState);
            }
        }
    }

    private static void updateColorSegments(Level level, BlockPos pos, BeaconLogicContainer.StateContainer beaconState) {
        beaconState.beamSegments.clear();
        if(!beaconState.isComplete()) {
            return;
        }
        int i = pos.getX();
        int j = pos.getY();
        int k = pos.getZ();
        Segment beamSegment = new Segment(beaconState.getEffect().getBeamColor().getTextureDiffuseColors());
        beaconState.beamSegments.add(beamSegment);
        boolean flag = true;
        BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos();
        for (int k1 = j + 1; k1 < level.getHeight(); ++k1) {
            BlockState iblockstate = level.getBlockState(blockpos$mutableblockpos.set(i, k1, k));
            Block block = iblockstate.getBlock();
            float[] afloat;

            if (block instanceof BeaconBeamBlock) {
                afloat = ((BeaconBeamBlock) block).getColor().getTextureDiffuseColors();


            } else {
                if (iblockstate.getLightBlock(level, blockpos$mutableblockpos) >= 15 && block != Blocks.BEDROCK) {
                    beaconState.setComplete(false);
                    beaconState.beamSegments.clear();
                    break;
                }
                beamSegment.increaseHeight();
                continue;
            }
            if (!flag) {
                afloat = new float[]{(beamSegment.getColor()[0] + afloat[0]) / 2.0F, (beamSegment.getColor()[1] + afloat[1]) / 2.0F, (beamSegment.getColor()[2] + afloat[2]) / 2.0F};
            }

            if (Arrays.equals(afloat, beamSegment.getColor())) {
                beamSegment.increaseHeight();
            } else {
                beamSegment = new Segment(afloat);
                beaconState.beamSegments.add(beamSegment);
            }
            flag = false;
        }
    }


    public static class Segment {
        final float[] color;
        private int height;

        public Segment(float[] fs) {
            this.color = fs;
            this.height = 1;
        }

        public void increaseHeight() {
            ++this.height;
        }

        public float[] getColor() {
            return this.color;
        }

        public int getHeight() {
            return this.height;
        }
    }

    public static class StateContainer {

        public final List<Segment> beamSegments = Lists.newArrayList();
        private BeaconEffect effect;
        private boolean isComplete;
        private int level;

        public StateContainer(BeaconEffect effect, boolean isComplete) {
            this.effect = effect;
            this.isComplete = isComplete;
        }

        public BeaconEffect getEffect() {
            return effect;
        }

        public void setEffect(BeaconEffect effect) {
            this.effect = effect;
        }

        public boolean isComplete() {
            return isComplete;
        }

        public void setComplete(boolean complete) {
            this.isComplete = complete;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public List<Segment> getBeamSegments() {
            return beamSegments;
        }

    }

    public static void loadStateContainer(CompoundTag tag, StateContainer container) {
        var effectTag = tag.getString("effect");
        if (!effectTag.isBlank()) {
            var effect = BeaconEffects.beaconEffects.get(new ResourceLocation(effectTag));
            if (effect != null) {
                container.setEffect(effect);
            }
            container.setLevel(tag.getInt("level"));
            container.setComplete(tag.getBoolean("complete"));
        }
    }

    public static void saveStateContainer(CompoundTag tag, StateContainer container) {
        if(container.getEffect() != null) {
            tag.putString("effect", container.getEffect().getTagResource().toString());
            tag.putInt("level", container.getLevel());
            tag.putBoolean("complete", container.isComplete());
        }
    }

}
