package com.primetoxinz.hardcore.beacons.logic.effects.fortune;

import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.TraceableEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;

public class FortuneEffectLogicContainer {
    public static ItemStack applyFortuneFromMobEffect(MobEffect effect, EffectApplyBonusCount.Formula formula, ItemStack itemStack, LootContext lootContext) {
        Entity entity = lootContext.getParamOrNull(LootContextParams.THIS_ENTITY);
        if(entity instanceof TraceableEntity traceableEntity) {
            entity = traceableEntity.getOwner();
        }

        if(entity instanceof LivingEntity livingEntity) {
            MobEffectInstance mobEffectInstance = livingEntity.getEffect(effect);
            if(mobEffectInstance != null) {
                int i = mobEffectInstance.getAmplifier();
                int j = formula.calculateNewCount(lootContext.getRandom(), itemStack.getCount(), i);
                itemStack.setCount(j);
            }
        }
        return itemStack;
    }
}
