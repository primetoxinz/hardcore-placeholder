package com.primetoxinz.hardcore.beacons.logic;

import com.google.common.collect.Maps;
import com.primetoxinz.hardcore.beacons.HardcoreBeacons;
import com.primetoxinz.hardcore.beacons.logic.effects.enderchest.EnderchestBeaconLogicContainer;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;

import java.util.Arrays;
import java.util.Map;

import static com.primetoxinz.hardcoreplaceholder.Common.MOD_ID;

public class BeaconEffects {
    public static final Map<ResourceLocation, BeaconEffect> beaconEffects = buildBeaconEffects();

    public static BeaconEffect newBeaconEffect(Map<ResourceLocation, BeaconEffect> registry, String name) {
        var effect = new BeaconEffect(new ResourceLocation(MOD_ID, name));
        registry.put(effect.getTagResource(), effect);
        return effect;
    }

    private static Map<ResourceLocation, BeaconEffect> buildBeaconEffects() {
        Map<ResourceLocation, BeaconEffect> beaconEffects = Maps.newHashMap();
        newBeaconEffect(beaconEffects, "cosmetic");

        newBeaconEffect(beaconEffects, "dig_speed")
                .withScalingPotionEffect(new MobEffectInstance(MobEffects.DIG_SPEED, 200, 0))
                .withBeamColor(DyeColor.YELLOW);

        newBeaconEffect(beaconEffects, "glowing")
                .withScalingPotionEffect(new MobEffectInstance(MobEffects.GLOWING, 200, 0))
                .withApplicationCriteria(beaconEffect -> entityLivingBase -> {
                    if (entityLivingBase instanceof Monster) {
//                        Optional<ForgeConfigSpec.ConfigValue> applicationChance = getConfigSpec(beaconEffect, "applicationChance");
//                        if(applicationChance.isPresent()) {
//                            return entityLivingBase.getRNG().nextInt(100) <= (int) applicationChance.get().get();
//                        }
                    }

                    return false;
                })
//                .setAddConfig((builder, configValues) -> {
//                    configValues.add(builder.defineInRange("applicationChance", 5, 0, 100));
//                })
                .withBeamColor(DyeColor.GRAY);

        //TODO - Finish these by adding their appropriate potion effects
        newBeaconEffect(beaconEffects, "looting")
                .withScalingPotionEffect(new MobEffectInstance(HardcoreBeacons.LOOTING_EFFECT.get(), 200, 0))
                .withBeamColor(DyeColor.GREEN);

        newBeaconEffect(beaconEffects, "fortune")
                .withScalingPotionEffect(new MobEffectInstance(HardcoreBeacons.FORTUNE_EFFECT.get(), 200, 0))
                .withBeamColor(DyeColor.LIGHT_BLUE);

        newBeaconEffect(beaconEffects, "night_vision")
                .withScalingPotionEffect(new MobEffectInstance(MobEffects.NIGHT_VISION, 200, 0))
                .withBeamColor(DyeColor.BLUE);

        newBeaconEffect(beaconEffects, "blindness")
                .withScalingPotionEffect(new MobEffectInstance(MobEffects.BLINDNESS, 200, 0))
                .withApplicationCriteria(BeaconLogicContainer.isPlayerNotWearing(EquipmentSlot.HEAD, itemStack -> itemStack.is(Items.NETHERITE_HELMET)))
                .withBeamColor(DyeColor.BLACK);


        newBeaconEffect(beaconEffects, "jump_boost")
                .withScalingPotionEffect(new MobEffectInstance(MobEffects.JUMP, 200, 0))
                .withBeamColor(DyeColor.GREEN);

        newBeaconEffect(beaconEffects, "fire_resist")
                .withScalingPotionEffect(new MobEffectInstance(MobEffects.FIRE_RESISTANCE, 200, 0))
                .setOnCreate((context) -> {
                    var currentLevel = context.state().getLevel();
                    var blockPos = context.beaconPos();
                    var world = context.level();
                    BlockPos firePosition;
                    for (int radius = 1; radius <= currentLevel; radius++) {
                        for (int x = -radius; x <= radius; x++) {
                            for (int z = -radius; z <= radius; z++) {
                                firePosition = blockPos.offset(x, -radius + 1, z);

                                if (world.getBlockState(firePosition).isAir()) {
                                    world.setBlock(firePosition, Blocks.FIRE.defaultBlockState(), 11);
                                }
                            }
                        }
                    }
                });

        newBeaconEffect(beaconEffects, "true_sight").withRange(4, 16*10).setOnApply(applyContext -> {
            var state = applyContext.state();
            var segments = state.beamSegments;
            var lastSegement = segments.get(segments.size()-1);
            var isPurple = isBeaconColorEqual(lastSegement.getColor(), DyeColor.PURPLE.getTextureDiffuseColors());
            if(isPurple) {
                BeaconEffect.getScalingPotion(new MobEffectInstance(HardcoreBeacons.WORLDSIGHT_EFFECT.get(), 200, 0, false, false, false, null, HardcoreBeacons.WORLDSIGHT_EFFECT.get().createFactorData())).accept(applyContext);
            } else {
                BeaconEffect.getScalingPotion(new MobEffectInstance(HardcoreBeacons.TRUESIGHT_EFFECT.get(), 200, 0, false, false, false, null, HardcoreBeacons.WORLDSIGHT_EFFECT.get().createFactorData())).accept(applyContext);
            }
        });


        newBeaconEffect(beaconEffects, "levitate")
                .withScalingPotionEffect(new MobEffectInstance(MobEffects.LEVITATION, 200, 0))
                .withRange(level -> level * 4);

        newBeaconEffect(beaconEffects, "enderchest").setOnApply(EnderchestBeaconLogicContainer::tick).setOnBreak(EnderchestBeaconLogicContainer::onRemove);

        return beaconEffects;
    }

    public static boolean isBeaconColorEqual(float[] a, float[] b) {
        return Arrays.equals(a,b);
    }
}
