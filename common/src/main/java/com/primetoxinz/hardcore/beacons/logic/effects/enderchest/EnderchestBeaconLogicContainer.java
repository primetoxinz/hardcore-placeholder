package com.primetoxinz.hardcore.beacons.logic.effects.enderchest;

import com.primetoxinz.common.api.IMenuProvider;
import com.primetoxinz.hardcore.beacons.game.features.enderchest.EnderChestBlockEntity;
import com.primetoxinz.hardcore.beacons.logic.BeaconEffect;
import com.primetoxinz.hardcoreplaceholder.Common;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.Container;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ChestMenu;
import net.minecraft.world.inventory.PlayerEnderChestContainer;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import org.apache.commons.compress.utils.Lists;

import java.util.ArrayList;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class EnderchestBeaconLogicContainer {
    private static final Component CONTAINER_TITLE = Component.translatable("container.enderchest");


    public static void setEnderchestType(int tier, EnderchestStateContainer container) {
        var channelType = StorageChannelType.VALUES[tier + 1];
        if (channelType != null) {
            container.setChannelType(channelType);
        }
    }
    public static void onEnderchest(BeaconEffect.Context context, BiConsumer<Integer, EnderchestStateContainer> consumer)   {
        var beaconLevel = context.state().getLevel();
        var pos = context.beaconPos();
        Level world = context.level();
        for (int r = -1; r < beaconLevel; r++) {
            for (int x = -(r + 1); x <= (r + 1); x++) {
                for (int z = -(r + 1); z <= (r + 1); z++) {
                    if (Math.abs(x) > r || Math.abs(z) > r) {
                        BlockPos p = pos.offset(x, -r, z);
                        BlockEntity tile = world.getBlockEntity(p);
                        if (tile instanceof EnderChestBlockEntity enderchest) {
                            consumer.accept(r, enderchest.getStateContainer());
                        }
                    }
                }
            }
        }
    }

    public static void tick(BeaconEffect.Context context) {
        Level world = context.level();
        if (world.getDayTime() % 40 == 0) {
            onEnderchest(context, EnderchestBeaconLogicContainer::setEnderchestType);
        }
    }

    public static void onRemove(BeaconEffect.Context context) {
        onEnderchest(context, (integer, enderchestStateContainer) -> enderchestStateContainer.setChannelType(StorageChannelType.NONE));
    }


    public static class EnderchestStateContainer implements IMenuProvider {

        public EnderchestStateContainer() {
            this.setChannelType(StorageChannelType.NONE);
        }

        public StorageChannelType getChannelType() {
            return channelType;
        }

        public void setChannelType(StorageChannelType channelType) {
            this.channelType = channelType;
        }

        private StorageChannelType channelType;


        @Override
        public MenuProvider getMenuProvider(Level level, BlockPos pos, Player player) {

            BlockEntity blockEntity = level.getBlockEntity(pos);
            if(blockEntity instanceof EnderChestBlockEntity enderchest) {
                PlayerEnderChestContainer container = null;
                switch(channelType) {
                    case DIMENSION_PUBLIC:
                    case DIMENSION_PUBLIC2:
                    case GLOBAL_PUBLIC:
                        container = getEnderChestContainer(channelType, level);
                        break;
                    case GLOBAL_PRIVATE:
                        container = player.getEnderChestInventory();
                        break;
                }
                if(container != null) {
                    container.setActiveChest(enderchest);
                    Container finalContainer = container;
                    return new SimpleMenuProvider((i, inventory, playerx) -> ChestMenu.threeRows(i, inventory, finalContainer), CONTAINER_TITLE);
                }
            }

            return null;
        }
    }


    public enum StorageChannelType {
        NONE(new ResourceLocation(Common.MOD_ID, "none")),
        DIMENSION_PUBLIC(new ResourceLocation(Common.MOD_ID, "dimension_public")),
        DIMENSION_PUBLIC2(new ResourceLocation(Common.MOD_ID, "dimension_public2")),
        GLOBAL_PUBLIC(new ResourceLocation(Common.MOD_ID, "global_public")),
        GLOBAL_PRIVATE(new ResourceLocation(Common.MOD_ID, "global_private"));

        private ResourceLocation name;

        StorageChannelType(ResourceLocation name) {
            this.name = name;
        }

        public ResourceLocation getName() {
            return name;
        }

        public static StorageChannelType[] VALUES = StorageChannelType.values();
    }

    public static PlayerEnderChestContainer getEnderChestContainer(StorageChannelType channelType, Level level) {
        var server = level.getServer();
        ResourceKey<Level> dimension = level.dimension();
        if(channelType == StorageChannelType.GLOBAL_PUBLIC) {
            dimension = Level.OVERWORLD;
        }
        var dataStorage = server.getLevel(dimension).getDataStorage();
        var data =  dataStorage.computeIfAbsent(EnderChestSavedData::fromTag, EnderChestSavedData::new, channelType.toString());
        return data.getEnderChestContainer();
    }

}
