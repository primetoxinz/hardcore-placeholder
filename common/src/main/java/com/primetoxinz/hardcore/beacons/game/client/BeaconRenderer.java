package com.primetoxinz.hardcore.beacons.game.client;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.world.level.block.entity.BeaconBlockEntity;
import net.minecraft.world.phys.Vec3;

public class BeaconRenderer<T extends BeaconBlockEntity> implements BlockEntityRenderer<T>{
    private final BlockEntityRenderer<BeaconBlockEntity> delegate;

    public BeaconRenderer(BlockEntityRendererProvider.Context context) {
        delegate = new net.minecraft.client.renderer.blockentity.BeaconRenderer(context);
    }

    @Override
    public void render(T blockEntity, float f, PoseStack poseStack, MultiBufferSource multiBufferSource, int i, int j) {
        delegate.render(blockEntity, f, poseStack, multiBufferSource, i,j);
    }

    @Override
    public boolean shouldRenderOffScreen(T blockEntity) {
        return delegate.shouldRenderOffScreen(blockEntity);
    }

    @Override
    public int getViewDistance() {
        return delegate.getViewDistance();
    }

    @Override
    public boolean shouldRender(T blockEntity, Vec3 vec3) {
        return delegate.shouldRender(blockEntity, vec3);
    }
}
