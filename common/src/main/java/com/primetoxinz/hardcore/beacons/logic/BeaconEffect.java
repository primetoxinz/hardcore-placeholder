package com.primetoxinz.hardcore.beacons.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.primetoxinz.common.MinecraftRegistries;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.phys.AABB;
import org.apache.logging.log4j.util.TriConsumer;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;
import java.util.function.*;

public class BeaconEffect {

    public static Consumer<Context> applyPotion(Map<Integer, List<MobEffectInstance>> potionEffects) {
        return (Context context) -> {
            if (context.livingEntity != null) {
                potionEffects.getOrDefault(context.state.getLevel(), Lists.newArrayList())
                        .forEach(potionEffect -> {
                            context.livingEntity.addEffect(new MobEffectInstance(potionEffect));
                        });
            }
        };
    }

    public static Consumer<Context> getScalingPotion(MobEffectInstance potionEffect) {
        Map<Integer, List<MobEffectInstance>> potionEffects = Maps.newHashMap();

        for (int level = 1; level <= 4; level++) {
            var p = new MobEffectInstance(potionEffect.getEffect(), potionEffect.getDuration(), potionEffect.getAmplifier() + level - 1, potionEffect.isAmbient(), potionEffect.isVisible(), potionEffect.showIcon(), null, potionEffect.getFactorData() );
            potionEffects.put(level, Lists.newArrayList(p));
        }
        return applyPotion(potionEffects);
    }

    private boolean enabled;
    private TagKey<Block> structureTag;

    private DyeColor beamColor;
    private Map<Integer, Integer> levelRanges;

    private Function<BeaconEffect, Predicate<LivingEntity>> applicationCriteria;
    private Map<Integer, List<MobEffectInstance>> potionEffects;
    private Consumer<Context> onCreate;
    private Consumer<Context> onApply;
    private Function<BeaconEffect, BiFunction<Level, BlockPos, TriConsumer<LivingEntity, InteractionHand, ItemStack>>> onInteract;
    private Consumer<Context> onBreak;


    public BeaconEffect(ResourceLocation tagLocation) {
        this.enabled = true;
        this.structureTag = TagKey.create(MinecraftRegistries.BLOCK, tagLocation);
        this.beamColor = DyeColor.WHITE;
        this.levelRanges = Maps.newHashMap();
        for (int level = 0; level <= 4; level++) {
            this.levelRanges.put(level, 20 * level);
        }
        this.applicationCriteria = beaconEffect -> entityLivingBase -> entityLivingBase instanceof Player;
        this.potionEffects = Maps.newHashMap();
        this.onCreate = context -> {
        };
        this.onApply = (Context context) -> {};
        this.onInteract = beaconEffect -> (world, blockPos) -> (entityLivingBase, enumHand, itemStack) -> {
        };
        this.onBreak = context -> {
        };
    }

    public void onCreate(Level world, BlockPos blockPos, BeaconLogicContainer.StateContainer stateContainer) {
        this.onCreate.accept(new Context(this, stateContainer, world, blockPos, null));
    }

    public void apply(Level world, BlockPos blockPos, BeaconLogicContainer.StateContainer stateContainer) {
        AABB rangeBB = new AABB(blockPos).inflate(levelRanges.get(stateContainer.getLevel()));
        List<LivingEntity> entitiesInRange = world.getEntitiesOfClass(LivingEntity.class, rangeBB);


        entitiesInRange.stream()
                .filter(entityLivingBase -> applicationCriteria.apply(this).test(entityLivingBase))
                .forEach(entityLivingBase -> {

                    this.onApply.accept(new Context(this, stateContainer, world, blockPos, entityLivingBase));
                });
    }

    public void onInteract(Level world, BlockPos blockPos, Player player, InteractionHand hand, ItemStack itemStack) {
        this.onInteract.apply(this).apply(world, blockPos).accept(player, hand, itemStack);
    }

    public void onBreak(Level world, BlockPos blockPos, BeaconLogicContainer.StateContainer stateContainer) {
        this.onBreak.accept(new Context(this, stateContainer, world, blockPos, null));
    }

    public BeaconEffect withRange(int level, int range) {
        this.levelRanges.put(level, range);
        return this;
    }

    public BeaconEffect withRange(Function<Integer, Integer> rangeFunc) {
        for (int level = 0; level < levelRanges.size(); level++) {
            this.levelRanges.put(level, rangeFunc.apply(level));
        }
        return this;
    }

    public BeaconEffect withPotionEffect(int level, MobEffectInstance potionEffect) {
        List<MobEffectInstance> effects = Lists.newArrayList(potionEffect);
        Map<Integer, List<MobEffectInstance>> potionEffects = Maps.newHashMap();
        potionEffects.put(level, effects);
        this.setOnApply(applyPotion(potionEffects));
        return this;
    }

    public BeaconEffect withScalingPotionEffect(MobEffectInstance potionEffect) {

        this.setOnApply(getScalingPotion(potionEffect));
        return this;
    }

    public BeaconEffect withApplicationCriteria(Function<BeaconEffect, Predicate<LivingEntity>> applicationCriteria) {
        this.applicationCriteria = applicationCriteria;
        return this;
    }

    public BeaconEffect withBeamColor(DyeColor beamColor) {
        this.beamColor = beamColor;
        return this;
    }

    public BeaconEffect setOnCreate(Consumer<Context> createFunc) {
        this.onCreate = createFunc;
        return this;
    }

    public BeaconEffect setOnApply(Consumer<Context> applyFunc) {
        this.onApply = applyFunc;
        return this;
    }

    public BeaconEffect setOnInteract(Function<BeaconEffect, BiFunction<Level, BlockPos, TriConsumer<LivingEntity, InteractionHand, ItemStack>>> interactFunc) {
        this.onInteract = interactFunc;
        return this;
    }

    public BeaconEffect setOnBreak(Consumer<Context> breakFunc) {
        this.onBreak = breakFunc;
        return this;
    }


    public DyeColor getBeamColor() {
        return beamColor;
    }

    public TagKey<Block> getStructureTag() {
        return structureTag;
    }

    public ResourceLocation getTagResource() {
        return structureTag.location();
    }


    public record Context(
            BeaconEffect beaconEffect,
            BeaconLogicContainer.StateContainer state,
            Level level,
            BlockPos beaconPos,
            @Nullable LivingEntity livingEntity
    ) {
    }
}
