package com.primetoxinz.hardcore.beacons.logic.effects.truesight;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;

public class TruesightEffectLogicContainer {

    private static int TRUESIGHT_RANGE = 10;

    public static void renderMobSpawnParticles(ClientLevel world, BlockPos blockPos) {
        BlockPos posUp = blockPos.above();
        BlockState state = world.getBlockState(blockPos);
        BlockState stateUp = world.getBlockState(posUp);

        if (MobSpawnHelpers.isBlocked(state, stateUp, world, blockPos, posUp)) {
            return;
        }


        int blockLightLevel = world.getBrightness(LightLayer.BLOCK, posUp);
        int skyLightLevel = world.getBrightness(LightLayer.SKY, posUp);

        var color = TrueSightColors.getARGBVec(blockLightLevel, skyLightLevel);

        double offset = MobSpawnHelpers.getOffset(stateUp, posUp, world);
        if (offset == -1f) {
            //can't spawn here
            return;
        }
        if (offset > 0.001f) {
            //if it renders above it should check if the block above culls the faces
            blockPos = blockPos.above();
        }

        var randomDecider = world.random.nextInt(blockLightLevel + 1);
        var shouldAddParticles = (blockLightLevel < 1) && randomDecider == 0;
        if (shouldAddParticles) {

            double x = blockPos.getX() + 0.5 + Mth.randomBetween(world.random, -0.5f, 0.5f);
            double y = posUp.getY();
            double z = blockPos.getZ() + 0.5 + Mth.randomBetween(world.random, -0.5f, 0.5f);

            world.addAlwaysVisibleParticle(ParticleTypes.ENTITY_EFFECT, x, y, z, color.x, color.y, color.z);
        }
    }
    public static void renderSpawnChunkParticles(ClientLevel world, BlockPos blockPos) {


        BlockPos posUp = blockPos.above();
        BlockState state = world.getBlockState(blockPos);
        BlockState stateUp = world.getBlockState(posUp);

        ChunkPos chunkPos = new ChunkPos(blockPos);
        ChunkPos worldSpawnChunk = new ChunkPos(world.getSharedSpawnPos());
        var chunkDistance = chunkPos.getChessboardDistance(worldSpawnChunk);

        if(!state.isAir()) {
            return;
        }

        if (chunkDistance <= 9) {

            var color = TrueSightColors.getSpawnChunkColor(chunkDistance == 9);
            var randomDecider = world.random.nextInt(25);

            var shouldAddParticles =   randomDecider == 0;
            if (shouldAddParticles) {
                double x = blockPos.getX() + 0.5 + Mth.randomBetween(world.random, -0.5f, 0.5f);
                double y = posUp.getY() + 0.5 + Mth.randomBetween(world.random, -0.5f, 0.5f);
                double z = blockPos.getZ() + 0.5 + Mth.randomBetween(world.random, -0.5f, 0.5f);

                world.addAlwaysVisibleParticle(ParticleTypes.ENTITY_EFFECT, x, y, z, color.x, color.y, color.z);
            }
        }

    }


    public static void truesightTicker(LivingEntity entity, int level) {
        if (entity != null && entity.level().isClientSide) {
            render(Minecraft.getInstance(), TruesightEffectLogicContainer::renderMobSpawnParticles);
        }
    }

    public static void worldsightTicker(LivingEntity entity, int level) {
        if (entity != null && entity.level().isClientSide) {
            render(Minecraft.getInstance(), TruesightEffectLogicContainer::renderSpawnChunkParticles);
        }
    }

    interface Renderer {
        void render(ClientLevel world, BlockPos pos);
    }

    public static void render(Minecraft minecraft, Renderer renderer) {

        if (minecraft.isPaused()) {
            return;
        }

        Player player = minecraft.player;
        ClientLevel world = minecraft.level;
        if (player == null || world == null) {
            return;
        }

        AABB playerBox = player.getBoundingBox();
        AABB truesightBox = playerBox.inflate(TRUESIGHT_RANGE);

        var blockPosStream = BlockPos.betweenClosedStream(truesightBox);
        blockPosStream.forEach(blockPos -> {
            renderer.render(world, blockPos);
        });
    }


}
