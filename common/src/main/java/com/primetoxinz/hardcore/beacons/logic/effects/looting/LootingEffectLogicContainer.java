package com.primetoxinz.hardcore.beacons.logic.effects.looting;

import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.TraceableEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.level.storage.loot.providers.number.NumberProvider;

public class LootingEffectLogicContainer {


    public static ItemStack applyLootingFromMobEffect(MobEffect effect, NumberProvider numberProvider, boolean hasLimit, int limit, ItemStack itemStack, LootContext lootContext) {
        Entity entity = lootContext.getParamOrNull(LootContextParams.KILLER_ENTITY);
        if(entity instanceof TraceableEntity traceableEntity) {
            entity = traceableEntity.getOwner();
        }
        if (entity instanceof LivingEntity livingEntity) {
            MobEffectInstance mobEffectInstance = livingEntity.getEffect(effect);
            if(mobEffectInstance != null) {
                int i = mobEffectInstance.getAmplifier();
                float f = (float) i * numberProvider.getFloat(lootContext);
                var count = Math.round(f);
                if (hasLimit) {
                    count = Math.min(limit, count);
                }
                itemStack.grow(count);
            }
        }
        return itemStack;
    }


}
