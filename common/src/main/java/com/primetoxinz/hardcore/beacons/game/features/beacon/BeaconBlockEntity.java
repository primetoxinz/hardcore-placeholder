package com.primetoxinz.hardcore.beacons.game.features.beacon;

import com.primetoxinz.hardcore.beacons.logic.BeaconLogicContainer;
import com.primetoxinz.hardcore.beacons.HardcoreBeacons;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Logger;

public class BeaconBlockEntity extends net.minecraft.world.level.block.entity.BeaconBlockEntity {


    private final BlockEntityType<BeaconBlockEntity> type;

    public BeaconLogicContainer.StateContainer getStateContainer() {
        return stateContainer;
    }

    private final BeaconLogicContainer.StateContainer stateContainer;

    public BeaconBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(blockPos, blockState);
        this.type = HardcoreBeacons.BEACON_BLOCK_ENTITY.get();
        this.stateContainer = new BeaconLogicContainer.StateContainer(null, false);
    }


    @Override
    public void setRemoved() {
        if (this.getLevel() != null) {
            BeaconLogicContainer.onRemoved(this.stateContainer, this.getLevel(), getBlockPos());
        }
        this.remove = true;
    }

    @Override
    public @NotNull BlockEntityType<BeaconBlockEntity> getType() {
        return this.type;
    }

    @Override
    public @NotNull List<BeaconBeamSection> getBeamSections() {
        return this.stateContainer.getBeamSegments().stream().map(segment -> newSegment(segment.getColor(), segment.getHeight())).toList();
    }

    @Override
    public ClientboundBlockEntityDataPacket getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public @NotNull CompoundTag getUpdateTag() {
        return this.saveWithoutMetadata();
    }

    @Override
    public void load(CompoundTag compoundTag) {
        BeaconLogicContainer.loadStateContainer(compoundTag, this.stateContainer);
    }

    @Override
    protected void saveAdditional(CompoundTag compoundTag) {
        BeaconLogicContainer.saveStateContainer(compoundTag, this.stateContainer);
    }

    public static BeaconBeamSection newSegment(float[] color, int height) {
        var section = new BeaconBeamSection(color);
        try {
            Field field = section.getClass().getDeclaredField("height");
            if (field.trySetAccessible()) {
                field.set(section, height);
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            Logger.getLogger("beacons").warning(e.toString());
            return null;
        }
        return section;
    }

}
