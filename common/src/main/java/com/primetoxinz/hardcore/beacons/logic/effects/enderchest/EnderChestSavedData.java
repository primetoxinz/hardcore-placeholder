package com.primetoxinz.hardcore.beacons.logic.effects.enderchest;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.inventory.PlayerEnderChestContainer;
import net.minecraft.world.level.saveddata.SavedData;

public class EnderChestSavedData extends SavedData {
    private final PlayerEnderChestContainer enderChestContainer;
    private static final String KEY = "EnderchestContainer";

    public EnderChestSavedData() {
        this(new PlayerEnderChestContainer());
    }

    public EnderChestSavedData(PlayerEnderChestContainer enderChestContainer) {
        this.enderChestContainer = enderChestContainer;

        this.enderChestContainer.addListener(container -> {
            setDirty();
        });
    }

    @Override
    public CompoundTag save(CompoundTag compoundTag) {

        compoundTag.put(KEY, enderChestContainer.createTag());

        return compoundTag;
    }

    public PlayerEnderChestContainer getEnderChestContainer() {
        return enderChestContainer;
    }

    public static EnderChestSavedData fromTag(CompoundTag tag) {
        var c = new PlayerEnderChestContainer();
        if (tag.contains(KEY, 9)) {
            c.fromTag(tag.getList(KEY, 10));
        }
        return new EnderChestSavedData(c);
    }
}
