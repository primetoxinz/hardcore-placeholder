package com.primetoxinz.hardcore.beacons.logic.effects.truesight;

import net.minecraft.world.phys.Vec3;

public class TrueSightColors {

    private static final int BLOCK_THRESHOLD = 0;
    private static final int SKY_THRESHOLD = 0;

    private static final int GREEN = 0x00FF00;
    private static final int BLACK = 0xFF000000;
    private static final int GRAY = 0xFF202020;

    public static int getSpawnChunkARGB() {
        return 0xFFFF00FF;
    }

    public static int getSafeARGB() {
        return GREEN;
    }

    public static int getWarningARGB() {
        return GRAY;
    }


    public static int getDangerARGB() {
        return BLACK;
    }




    public static Vec3 getColorVec(int m) {
        var n = (double)((float)(m >> 16 & 255) / 255.0F);
        var o = (double)((float)(m >> 8 & 255) / 255.0F);
        var p = (double)((float)(m & 255) / 255.0F);
        return new Vec3(n,o,p);
    }

    public static Vec3 getARGBVec(int blockLightLevel, int skyLightLevel) {

        int m = getMobSpawnColor(blockLightLevel, skyLightLevel);
        return getColorVec(m);
    }

    public static int getMobSpawnColor(int blockLightLevel, int skyLightLevel) {
        int color = getSafeARGB();

        if (blockLightLevel <= BLOCK_THRESHOLD) {
            if (skyLightLevel <= SKY_THRESHOLD) {
                color = getDangerARGB();
            } else {
                color = getWarningARGB();
            }
        }
        return color;
    }


    public static Vec3 getSpawnChunkColor(boolean isEdge) {
        if (isEdge) {
             return getColorVec(0x00007F);
        }
        return getColorVec(0x7F007F);
    }


}
