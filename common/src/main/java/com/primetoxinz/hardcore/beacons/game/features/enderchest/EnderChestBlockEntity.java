package com.primetoxinz.hardcore.beacons.game.features.enderchest;

import com.primetoxinz.common.api.IMenuProvider;
import com.primetoxinz.hardcore.beacons.HardcoreBeacons;
import com.primetoxinz.hardcore.beacons.logic.effects.enderchest.EnderchestBeaconLogicContainer;
import net.minecraft.core.BlockPos;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.Container;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ChestMenu;
import net.minecraft.world.inventory.PlayerEnderChestContainer;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.ChestLidController;
import net.minecraft.world.level.block.entity.ContainerOpenersCounter;
import net.minecraft.world.level.block.entity.LidBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class EnderChestBlockEntity extends net.minecraft.world.level.block.entity.EnderChestBlockEntity implements LidBlockEntity, IMenuProvider {

    private final EnderchestBeaconLogicContainer.EnderchestStateContainer stateContainer = new EnderchestBeaconLogicContainer.EnderchestStateContainer();


    private final ChestLidController chestLidController = new ChestLidController();
    private final ContainerOpenersCounter openersCounter = new ContainerOpenersCounter() {
        protected void onOpen(Level level, BlockPos blockPos, BlockState blockState) {
            level.playSound((Player)null, (double)blockPos.getX() + 0.5, (double)blockPos.getY() + 0.5, (double)blockPos.getZ() + 0.5, SoundEvents.ENDER_CHEST_OPEN, SoundSource.BLOCKS, 0.5F, level.random.nextFloat() * 0.1F + 0.9F);
        }

        protected void onClose(Level level, BlockPos blockPos, BlockState blockState) {
            level.playSound((Player)null, (double)blockPos.getX() + 0.5, (double)blockPos.getY() + 0.5, (double)blockPos.getZ() + 0.5, SoundEvents.ENDER_CHEST_CLOSE, SoundSource.BLOCKS, 0.5F, level.random.nextFloat() * 0.1F + 0.9F);
        }

        protected void openerCountChanged(Level level, BlockPos blockPos, BlockState blockState, int i, int j) {
            level.blockEvent(getBlockPos(), HardcoreBeacons.ENDER_CHEST_BLOCK.get(), 1, j);
        }

        protected boolean isOwnContainer(Player player) {
            var b =  player.getEnderChestInventory().isActiveChest(EnderChestBlockEntity.this);;
            return b;
        }
    };

    public EnderChestBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(blockPos, blockState);
    }

    @Override
    public BlockEntityType<?> getType() {
        return HardcoreBeacons.ENDER_CHEST_BLOCK_ENTITY.get();
    }

    public static void lidAnimateTick(Level level, BlockPos blockPos, BlockState blockState, EnderChestBlockEntity enderChestBlockEntity) {
        enderChestBlockEntity.chestLidController.tickLid();
    }

    public boolean triggerEvent(int i, int j) {
        if (i == 1) {
            this.chestLidController.shouldBeOpen(j > 0);
            return true;
        } else {
            return super.triggerEvent(i, j);
        }
    }

    public void startOpen(Player player) {
        if (!this.remove && !player.isSpectator()) {
            this.openersCounter.incrementOpeners(player, this.getLevel(), this.getBlockPos(), this.getBlockState());
        }

     }

    public void stopOpen(Player player) {
        if (!this.remove && !player.isSpectator()) {
            this.openersCounter.decrementOpeners(player, this.getLevel(), this.getBlockPos(), this.getBlockState());
        }

    }

    public boolean stillValid(Player player) {
        return Container.stillValidBlockEntity(this, player);
    }

    public void recheckOpen() {
        if (!this.remove) {
            this.openersCounter.recheckOpeners(this.getLevel(), this.getBlockPos(), this.getBlockState());
        }

    }

    public float getOpenNess(float f) {
        return this.chestLidController.getOpenness(f);
    }

    public MenuProvider getMenuProvider(Level level, BlockPos pos, Player player) {
        return stateContainer.getMenuProvider(level, pos, player);
    }

    public EnderchestBeaconLogicContainer.EnderchestStateContainer getStateContainer() {
        return stateContainer;
    }

}
