package com.primetoxinz.hardcore.beacons.logic.effects.fortune;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.primetoxinz.hardcore.beacons.HardcoreBeacons;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.GsonHelper;
import net.minecraft.util.RandomSource;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.functions.LootItemConditionalFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;
import net.minecraft.world.level.storage.loot.parameters.LootContextParam;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Set;

public class EffectApplyBonusCount extends LootItemConditionalFunction {
    static final Map<ResourceLocation, EffectApplyBonusCount.FormulaDeserializer> FORMULAS = Maps.newHashMap();
    final MobEffect mobEffect;
    final Formula formula;

    EffectApplyBonusCount(LootItemCondition[] lootItemConditions, MobEffect mobEffect, EffectApplyBonusCount.Formula formula) {
        super(lootItemConditions);
        this.mobEffect = mobEffect;
        this.formula = formula;
    }

    public @NotNull LootItemFunctionType getType() {
        return HardcoreBeacons.EFFECT_BONUS.get();
    }

    public @NotNull Set<LootContextParam<?>> getReferencedContextParams() {
        return ImmutableSet.of(LootContextParams.THIS_ENTITY);
    }

    public @NotNull ItemStack run(ItemStack itemStack, LootContext lootContext) {
        return FortuneEffectLogicContainer.applyFortuneFromMobEffect(this.mobEffect, this.formula, itemStack, lootContext);
    }

    public static LootItemConditionalFunction.Builder<?> addBonusBinomialDistributionCount(MobEffect mobEffect, float f, int i) {
        return simpleBuilder((lootItemConditions) -> {
            return new EffectApplyBonusCount(lootItemConditions, mobEffect, new EffectApplyBonusCount.BinomialWithBonusCount(i, f));
        });
    }

    public static LootItemConditionalFunction.Builder<?> addOreBonusCount(MobEffect mobEffect) {
        return simpleBuilder((lootItemConditions) -> {
            return new EffectApplyBonusCount(lootItemConditions, mobEffect, new OreDrops());
        });
    }

    public static LootItemConditionalFunction.Builder<?> addUniformBonusCount(MobEffect mobEffect) {
        return simpleBuilder((lootItemConditions) -> {
            return new EffectApplyBonusCount(lootItemConditions, mobEffect, new UniformBonusCount(1));
        });
    }

    public static LootItemConditionalFunction.Builder<?> addUniformBonusCount(MobEffect mobEffect, int i) {
        return simpleBuilder((lootItemConditions) -> {
            return new EffectApplyBonusCount(lootItemConditions, mobEffect, new UniformBonusCount(i));
        });
    }

    static {
        FORMULAS.put(EffectApplyBonusCount.BinomialWithBonusCount.TYPE, EffectApplyBonusCount.BinomialWithBonusCount::deserialize);
        FORMULAS.put(EffectApplyBonusCount.OreDrops.TYPE, EffectApplyBonusCount.OreDrops::deserialize);
        FORMULAS.put(EffectApplyBonusCount.UniformBonusCount.TYPE, EffectApplyBonusCount.UniformBonusCount::deserialize);
    }

    public interface Formula {
        int calculateNewCount(RandomSource randomSource, int i, int j);

        void serializeParams(JsonObject jsonObject, JsonSerializationContext jsonSerializationContext);

        ResourceLocation getType();
    }

    private static final class UniformBonusCount implements EffectApplyBonusCount.Formula {
        public static final ResourceLocation TYPE = new ResourceLocation("uniform_bonus_count");
        private final int bonusMultiplier;

        public UniformBonusCount(int i) {
            this.bonusMultiplier = i;
        }

        public int calculateNewCount(RandomSource randomSource, int i, int j) {
            return i + randomSource.nextInt(this.bonusMultiplier * j + 1);
        }

        public void serializeParams(JsonObject jsonObject, JsonSerializationContext jsonSerializationContext) {
            jsonObject.addProperty("bonusMultiplier", this.bonusMultiplier);
        }

        public static EffectApplyBonusCount.Formula deserialize(JsonObject jsonObject, JsonDeserializationContext jsonDeserializationContext) {
            int i = GsonHelper.getAsInt(jsonObject, "bonusMultiplier");
            return new EffectApplyBonusCount.UniformBonusCount(i);
        }

        public ResourceLocation getType() {
            return TYPE;
        }
    }

    private static final class OreDrops implements EffectApplyBonusCount.Formula {
        public static final ResourceLocation TYPE = new ResourceLocation("ore_drops");

        OreDrops() {
        }

        public int calculateNewCount(RandomSource randomSource, int i, int j) {
            if (j > 0) {
                int k = randomSource.nextInt(j + 2) - 1;
                if (k < 0) {
                    k = 0;
                }

                return i * (k + 1);
            } else {
                return i;
            }
        }

        public void serializeParams(JsonObject jsonObject, JsonSerializationContext jsonSerializationContext) {
        }

        public static EffectApplyBonusCount.Formula deserialize(JsonObject jsonObject, JsonDeserializationContext jsonDeserializationContext) {
            return new EffectApplyBonusCount.OreDrops();
        }

        public ResourceLocation getType() {
            return TYPE;
        }
    }

    private static final class BinomialWithBonusCount implements EffectApplyBonusCount.Formula {
        public static final ResourceLocation TYPE = new ResourceLocation("binomial_with_bonus_count");
        private final int extraRounds;
        private final float probability;

        public BinomialWithBonusCount(int i, float f) {
            this.extraRounds = i;
            this.probability = f;
        }

        public int calculateNewCount(RandomSource randomSource, int i, int j) {
            for(int k = 0; k < j + this.extraRounds; ++k) {
                if (randomSource.nextFloat() < this.probability) {
                    ++i;
                }
            }

            return i;
        }

        public void serializeParams(JsonObject jsonObject, JsonSerializationContext jsonSerializationContext) {
            jsonObject.addProperty("extra", this.extraRounds);
            jsonObject.addProperty("probability", this.probability);
        }

        public static EffectApplyBonusCount.Formula deserialize(JsonObject jsonObject, JsonDeserializationContext jsonDeserializationContext) {
            int i = GsonHelper.getAsInt(jsonObject, "extra");
            float f = GsonHelper.getAsFloat(jsonObject, "probability");
            return new EffectApplyBonusCount.BinomialWithBonusCount(i, f);
        }

        public ResourceLocation getType() {
            return TYPE;
        }
    }

    private interface FormulaDeserializer {
        EffectApplyBonusCount.Formula deserialize(JsonObject jsonObject, JsonDeserializationContext jsonDeserializationContext);
    }

    public static class Serializer extends LootItemConditionalFunction.Serializer<EffectApplyBonusCount> {
        public Serializer() {
        }

        public void serialize(JsonObject jsonObject, EffectApplyBonusCount applyBonusCount, JsonSerializationContext jsonSerializationContext) {
            super.serialize(jsonObject, applyBonusCount, jsonSerializationContext);
            jsonObject.addProperty("effect", BuiltInRegistries.MOB_EFFECT.getKey(applyBonusCount.mobEffect).toString());
            jsonObject.addProperty("formula", applyBonusCount.formula.getType().toString());
            JsonObject jsonObject2 = new JsonObject();
            applyBonusCount.formula.serializeParams(jsonObject2, jsonSerializationContext);
            if (jsonObject2.size() > 0) {
                jsonObject.add("parameters", jsonObject2);
            }

        }

        public EffectApplyBonusCount deserialize(JsonObject jsonObject, JsonDeserializationContext jsonDeserializationContext, LootItemCondition[] lootItemConditions) {
            ResourceLocation resourceLocation = new ResourceLocation(GsonHelper.getAsString(jsonObject, "effect"));
            MobEffect mobEffect = (MobEffect) BuiltInRegistries.MOB_EFFECT.getOptional(resourceLocation).orElseThrow(() -> {
                return new JsonParseException("Invalid effect id: " + resourceLocation);
            });
            ResourceLocation resourceLocation2 = new ResourceLocation(GsonHelper.getAsString(jsonObject, "formula"));
            FormulaDeserializer formulaDeserializer = (FormulaDeserializer)FORMULAS.get(resourceLocation2);
            if (formulaDeserializer == null) {
                throw new JsonParseException("Invalid formula id: " + resourceLocation2);
            } else {
                Formula formula;
                if (jsonObject.has("parameters")) {
                    formula = formulaDeserializer.deserialize(GsonHelper.getAsJsonObject(jsonObject, "parameters"), jsonDeserializationContext);
                } else {
                    formula = formulaDeserializer.deserialize(new JsonObject(), jsonDeserializationContext);
                }

                return new EffectApplyBonusCount(lootItemConditions, mobEffect, formula);
            }
        }
    }
}
