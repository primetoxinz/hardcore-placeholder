package com.primetoxinz.hardcore.beacons;

import com.primetoxinz.common.CustomMobEffect;
import com.primetoxinz.common.MinecraftRegistries;
import com.primetoxinz.common.resources.ResourcePackHook;
import com.primetoxinz.hardcore.beacons.game.features.beacon.BeaconBlock;
import com.primetoxinz.hardcore.beacons.game.features.beacon.BeaconBlockEntity;
import com.primetoxinz.hardcore.beacons.game.features.enderchest.EnderChestBlock;
import com.primetoxinz.hardcore.beacons.game.features.enderchest.EnderChestBlockEntity;
import com.primetoxinz.hardcore.beacons.logic.effects.fortune.EffectApplyBonusCount;
import com.primetoxinz.hardcore.beacons.logic.effects.looting.LootItemRandomChanceWithLootingCondition;
import com.primetoxinz.hardcore.beacons.logic.effects.looting.LootingEffectFunction;
import com.primetoxinz.hardcore.beacons.logic.effects.truesight.TruesightEffectLogicContainer;
import dev.architectury.platform.Platform;
import dev.architectury.registry.registries.DeferredRegister;
import dev.architectury.registry.registries.RegistrySupplier;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectCategory;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.properties.NoteBlockInstrument;
import net.minecraft.world.level.material.MapColor;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;
import net.minecraft.world.level.storage.loot.predicates.LootItemConditionType;

import static com.primetoxinz.hardcoreplaceholder.Common.MOD_ID;

public class HardcoreBeacons {

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(MOD_ID, MinecraftRegistries.BLOCK);
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(MOD_ID, MinecraftRegistries.ITEM);
    public static final DeferredRegister<BlockEntityType<?>> BLOCK_ENTITY_TYPE = DeferredRegister.create(MOD_ID, MinecraftRegistries.BLOCK_ENTITY_TYPE);

    public static final RegistrySupplier<Block> BEACON_BLOCK = BLOCKS.register("beacon", () ->
            new BeaconBlock(
                    BlockBehaviour.Properties.of().mapColor(MapColor.DIAMOND).instrument(NoteBlockInstrument.HAT).strength(3.0F).lightLevel((blockStatex) -> 15).noOcclusion().isRedstoneConductor((blockState, blockGetter, blockPos) -> false)
            )
    );
    public static final RegistrySupplier<Item> BEACON_BLOCK_ITEM = ITEMS.register("beacon", () ->
            new BlockItem(BEACON_BLOCK.get(), new Item.Properties().arch$tab(CreativeModeTabs.FUNCTIONAL_BLOCKS))
    );
    public static final RegistrySupplier<BlockEntityType<BeaconBlockEntity>> BEACON_BLOCK_ENTITY = BLOCK_ENTITY_TYPE.register(
            "beacon",
            () -> BlockEntityType.Builder.of(BeaconBlockEntity::new, BEACON_BLOCK.get()).build(null)
    );

    public static final RegistrySupplier<Block> ENDER_CHEST_BLOCK = BLOCKS.register("ender_chest", () -> new EnderChestBlock(BlockBehaviour.Properties.of().mapColor(MapColor.STONE).instrument(NoteBlockInstrument.BASEDRUM).requiresCorrectToolForDrops().strength(22.5F, 600.0F).lightLevel((blockStatex) -> 7)));
    public static final RegistrySupplier<Item> ENDER_CHEST_BLOCK_ITEM = ITEMS.register("ender_chest", () ->
            new BlockItem(ENDER_CHEST_BLOCK.get(), new Item.Properties().arch$tab(CreativeModeTabs.FUNCTIONAL_BLOCKS))
    );
    public static final RegistrySupplier<BlockEntityType<EnderChestBlockEntity>> ENDER_CHEST_BLOCK_ENTITY = BLOCK_ENTITY_TYPE.register(
            "ender_chest",
            () -> BlockEntityType.Builder.of(EnderChestBlockEntity::new, ENDER_CHEST_BLOCK.get()).build(null)
    );

    public static final DeferredRegister<LootItemConditionType> LOOT_CONDITION_TYPE = DeferredRegister.create(MOD_ID, MinecraftRegistries.LOOT_CONDITION_TYPE);
    public static final RegistrySupplier<LootItemConditionType> LOOTING_EFFECT_CONDITION = LOOT_CONDITION_TYPE.register("random_chance_with_looting_effect", () -> new LootItemConditionType(new LootItemRandomChanceWithLootingCondition.Serializer()));
    public static final DeferredRegister<LootItemFunctionType> LOOT_FUNCTION_TYPE = DeferredRegister.create(MOD_ID, MinecraftRegistries.LOOT_FUNCTION_TYPE);

    public static final RegistrySupplier<LootItemFunctionType> EFFECT_BONUS = LOOT_FUNCTION_TYPE.register("effect_bonus", () -> new LootItemFunctionType(new EffectApplyBonusCount.Serializer()));
    public static final RegistrySupplier<LootItemFunctionType> LOOTING_EFFECT_BONUS = LOOT_FUNCTION_TYPE.register("looting_effect", () -> new LootItemFunctionType(new LootingEffectFunction.Serializer()));
    public static final DeferredRegister<MobEffect> MOB_EFFECT = DeferredRegister.create(MOD_ID, MinecraftRegistries.MOB_EFFECT);
    public static final RegistrySupplier<MobEffect> FORTUNE_EFFECT = MOB_EFFECT.register("fortune", () -> new CustomMobEffect(MobEffectCategory.BENEFICIAL, 5882118));
    public static final RegistrySupplier<MobEffect> LOOTING_EFFECT = MOB_EFFECT.register("looting", () -> new CustomMobEffect(MobEffectCategory.BENEFICIAL, 5882118));
    public static final RegistrySupplier<MobEffect> TRUESIGHT_EFFECT = MOB_EFFECT.register("truesight", () -> new CustomMobEffect(MobEffectCategory.BENEFICIAL, 5882118).setTicker(TruesightEffectLogicContainer::truesightTicker));
    public static final RegistrySupplier<MobEffect> WORLDSIGHT_EFFECT = MOB_EFFECT.register("worldsight", () -> new CustomMobEffect(MobEffectCategory.BENEFICIAL, 5882118).setTicker(TruesightEffectLogicContainer::worldsightTicker));

    public static void init() {
        BLOCKS.register();
        BLOCK_ENTITY_TYPE.register();
        ITEMS.register();
        LOOT_FUNCTION_TYPE.register();
        LOOT_CONDITION_TYPE.register();
        MOB_EFFECT.register();

        ResourcePackHook.registerBuiltinResourcePack(new ResourcePackHook.BuiltinResourcePack(
                new ResourceLocation(MOD_ID, "hcbeacons"), Component.translatable("modules.hcbeacons.name"), ResourcePackHook.ResourcePackActivationType.DEFAULT_ENABLED,
                Platform.getMod(MOD_ID)
        ));
    }


}
