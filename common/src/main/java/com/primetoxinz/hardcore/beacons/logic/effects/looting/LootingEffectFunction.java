package com.primetoxinz.hardcore.beacons.logic.effects.looting;


import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import java.util.Set;

import com.primetoxinz.hardcore.beacons.HardcoreBeacons;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.functions.LootItemConditionalFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;
import net.minecraft.world.level.storage.loot.parameters.LootContextParam;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.providers.number.NumberProvider;

public class LootingEffectFunction extends LootItemConditionalFunction {
    public static final int NO_LIMIT = 0;
    final NumberProvider value;
    final int limit;

    LootingEffectFunction(LootItemCondition[] lootItemConditions, NumberProvider numberProvider, int i) {
        super(lootItemConditions);
        this.value = numberProvider;
        this.limit = i;
    }

    public LootItemFunctionType getType() {
        return HardcoreBeacons.LOOTING_EFFECT_BONUS.get();
    }

    public Set<LootContextParam<?>> getReferencedContextParams() {
        return Sets.union(ImmutableSet.of(LootContextParams.KILLER_ENTITY), this.value.getReferencedContextParams());
    }

    boolean hasLimit() {
        return this.limit > 0;
    }

    public ItemStack run(ItemStack itemStack, LootContext lootContext) {
        return LootingEffectLogicContainer.applyLootingFromMobEffect(HardcoreBeacons.LOOTING_EFFECT.get(), this.value, this.hasLimit(), this.limit, itemStack, lootContext);
    }

    public static LootingEffectFunction.Builder lootingMultiplier(NumberProvider numberProvider) {
        return new LootingEffectFunction.Builder(numberProvider);
    }

    public static class Builder extends LootItemConditionalFunction.Builder<LootingEffectFunction.Builder> {
        private final NumberProvider count;
        private int limit = 0;

        public Builder(NumberProvider numberProvider) {
            this.count = numberProvider;
        }

        protected LootingEffectFunction.Builder getThis() {
            return this;
        }

        public LootingEffectFunction.Builder setLimit(int i) {
            this.limit = i;
            return this;
        }

        public LootItemFunction build() {
            return new LootingEffectFunction(this.getConditions(), this.count, this.limit);
        }
    }

    public static class Serializer extends LootItemConditionalFunction.Serializer<LootingEffectFunction> {
        public Serializer() {
        }

        public void serialize(JsonObject jsonObject, LootingEffectFunction LootingEffectFunction, JsonSerializationContext jsonSerializationContext) {
            super.serialize(jsonObject, LootingEffectFunction, jsonSerializationContext);
            jsonObject.add("count", jsonSerializationContext.serialize(LootingEffectFunction.value));
            if (LootingEffectFunction.hasLimit()) {
                jsonObject.add("limit", jsonSerializationContext.serialize(LootingEffectFunction.limit));
            }

        }

        public LootingEffectFunction deserialize(JsonObject jsonObject, JsonDeserializationContext jsonDeserializationContext, LootItemCondition[] lootItemConditions) {
            int i = GsonHelper.getAsInt(jsonObject, "limit", 0);
            return new LootingEffectFunction(lootItemConditions, (NumberProvider) GsonHelper.getAsObject(jsonObject, "count", jsonDeserializationContext, NumberProvider.class), i);
        }
    }
}

