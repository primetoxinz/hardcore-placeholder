package com.primetoxinz.hardcoreplaceholder;

import com.primetoxinz.hardcore.beacons.HardcoreBeacons;
import dev.architectury.hooks.PackRepositoryHooks;

public class Common
{
	public static final String MOD_ID = "hardcoreplaceholder";
	
	public static void init() {
		HardcoreBeacons.init();
//		PackRepositoryHooks.addSource();
	}
}
