package com.primetoxinz.hardcoreplaceholder;

import com.primetoxinz.hardcore.beacons.HardcoreBeacons;
import com.primetoxinz.hardcore.beacons.game.client.BeaconRenderer;
import dev.architectury.registry.client.rendering.BlockEntityRendererRegistry;
import dev.architectury.registry.client.rendering.RenderTypeRegistry;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.blockentity.ChestRenderer;

public class Client {

    public static void init() {
        BlockEntityRendererRegistry.register(HardcoreBeacons.BEACON_BLOCK_ENTITY.get(), BeaconRenderer::new);
        BlockEntityRendererRegistry.register(HardcoreBeacons.ENDER_CHEST_BLOCK_ENTITY.get(), ChestRenderer::new);
        RenderTypeRegistry.register(RenderType.translucent(), HardcoreBeacons.BEACON_BLOCK.get());
    }

}
