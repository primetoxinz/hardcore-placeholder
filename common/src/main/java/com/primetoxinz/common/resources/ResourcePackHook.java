package com.primetoxinz.common.resources;

import dev.architectury.injectables.annotations.ExpectPlatform;
import dev.architectury.platform.Mod;
import net.fabricmc.loader.api.ModContainer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;

import java.util.List;

public class ResourcePackHook {

    public enum ResourcePackActivationType {
        /**
         * Normal activation. The user has full control over the activation of the resource pack.
         */
        NORMAL,
        /**
         * Enabled by default. The user has still full control over the activation of the resource pack.
         */
        DEFAULT_ENABLED,
        /**
         * Always enabled. The user cannot disable the resource pack.
         */
        ALWAYS_ENABLED;

        /**
         * Returns whether this resource pack will be enabled by default or not.
         *
         * @return {@code true} if enabled by default, else {@code false}
         */
        public boolean isEnabledByDefault() {
            return this == DEFAULT_ENABLED || this == ALWAYS_ENABLED;
        }
    }

    @ExpectPlatform
    public static void registerBuiltinResourcePack(BuiltinResourcePack pack) {
    }

    public static void registerBuiltinResourcePacks() {
        for (BuiltinResourcePack builtinResourcePack : builtinResourcePacks) {
            registerBuiltinResourcePack(builtinResourcePack);
        }
    }

    public static List<BuiltinResourcePack> builtinResourcePacks = List.of();


    public static void addBuiltinResourcePack(BuiltinResourcePack pack) {
        builtinResourcePacks.add(pack);
    }

    public record BuiltinResourcePack(ResourceLocation id, Component displayName, ResourcePackActivationType activationType, Mod mod) {}

}
