package com.primetoxinz.common;

import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectCategory;
import net.minecraft.world.entity.LivingEntity;

public class CustomMobEffect extends MobEffect {
    private Ticker ticker;

    public CustomMobEffect(MobEffectCategory mobEffectCategory, int i) {
        super(mobEffectCategory, i);
    }

    public interface Ticker {
        void tick(LivingEntity livingEntity, int level);
    }

    public CustomMobEffect setTicker(Ticker ticker) {
        this.ticker = ticker;
        return this;
    }

    @Override
    public void applyEffectTick(LivingEntity livingEntity, int i) {
        if (ticker != null) {
            ticker.tick(livingEntity, i);
        }
    }

    @Override
    public boolean isDurationEffectTick(int i, int j) {
        return ticker != null;
    }
}
