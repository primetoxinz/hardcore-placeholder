package com.primetoxinz.common.api;

import net.minecraft.core.BlockPos;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;

public interface IMenuProvider {

    MenuProvider getMenuProvider(Level level, BlockPos pos, Player player);

}
