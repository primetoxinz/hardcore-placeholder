package com.primetoxinz.common;

import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.storage.loot.functions.LootItemConditionalFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;
import net.minecraft.world.level.storage.loot.predicates.LootItemConditionType;

public class MinecraftRegistries
{

    public static final ResourceKey<Registry<Block>> BLOCK = Registries.BLOCK;
    public static final ResourceKey<Registry<BlockEntityType<?>>> BLOCK_ENTITY_TYPE = Registries.BLOCK_ENTITY_TYPE;
    public static final ResourceKey<Registry<Item>> ITEM = Registries.ITEM;
    public static final ResourceKey<Registry<LootItemFunctionType>> LOOT_FUNCTION_TYPE = Registries.LOOT_FUNCTION_TYPE;
    public static final ResourceKey<Registry<LootItemConditionType>> LOOT_CONDITION_TYPE = Registries.LOOT_CONDITION_TYPE;
    public static final ResourceKey<Registry<MobEffect>> MOB_EFFECT = Registries.MOB_EFFECT;

}
