package com.primetoxinz.common.resources.fabric;

import com.google.common.collect.Maps;
import com.primetoxinz.common.resources.ResourcePackHook;
import com.primetoxinz.hardcoreplaceholder.Common;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.fabric.api.resource.ResourcePackActivationType;
import net.fabricmc.loader.api.FabricLoader;

import java.util.EnumMap;

public class ResourcePackHookImpl {

    private static final EnumMap<ResourcePackHook.ResourcePackActivationType, ResourcePackActivationType> convert = Maps.newEnumMap(ResourcePackHook.ResourcePackActivationType.class);
    static {
        for(ResourcePackHook.ResourcePackActivationType value: ResourcePackHook.ResourcePackActivationType.values()) {
            convert.put(value, ResourcePackActivationType.valueOf(value.name()));
        }
    }

    public static void registerBuiltinResourcePack(ResourcePackHook.BuiltinResourcePack pack) {
        FabricLoader.getInstance().getModContainer(Common.MOD_ID).ifPresent(modContainer -> {
            ResourceManagerHelper.registerBuiltinResourcePack(pack.id(), modContainer, pack.displayName(), convert.get(pack.activationType()));
        });
    }
}
