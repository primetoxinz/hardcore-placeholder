package com.primetoxinz.hardcoreplaceholder.fabric;

import com.primetoxinz.hardcoreplaceholder.Common;
import net.fabricmc.api.ModInitializer;

public class HardcorePlaceholderFabric implements ModInitializer {
    @Override
    public void onInitialize() {
        Common.init();
    }
}