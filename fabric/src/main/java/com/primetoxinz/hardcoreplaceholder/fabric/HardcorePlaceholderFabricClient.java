package com.primetoxinz.hardcoreplaceholder.fabric;

import com.primetoxinz.hardcoreplaceholder.Client;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.ModInitializer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.level.Level;

public class HardcorePlaceholderFabricClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        Client.init();
    }


}