package com.primetoxinz.common.resources.forge;

import com.primetoxinz.common.resources.ResourcePackHook;
import net.minecraft.server.packs.PathPackResources;
import net.minecraft.server.packs.repository.PackSource;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.AddPackFindersEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.Logging;
import net.minecraftforge.fml.ModList;
import net.minecraft.server.packs.repository.Pack;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import net.minecraft.network.chat.Component;

public class ResourcePackHookImpl {

    public static final List<ResourcePackHook.BuiltinResourcePack> builtinResourcePacks = new ArrayList<>();

    public static void addPackFinders(AddPackFindersEvent event) {
        for (ResourcePackHook.BuiltinResourcePack builtinResourcePack : builtinResourcePacks) {
            registerBuiltinResourcePack(event, builtinResourcePack);
        }
    }

    private static Logger LOGGER = Logger.getLogger("ResourcePackHookImpl");

    public static void registerBuiltinResourcePack(AddPackFindersEvent event, ResourcePackHook.BuiltinResourcePack builtinResourcePack) {
        var modFile = ModList.get().getModFileById(builtinResourcePack.mod().getModId()).getFile();
        var fs = modFile.getFilePath().getFileSystem();

        var resourcePath = modFile.findResource(fs.getPath("resourcepacks", builtinResourcePack.id().getPath()).toString());

        var pack = Pack.readMetaAndCreate(
                builtinResourcePack.id().toString(),
                builtinResourcePack.displayName(),
                false,
                (path) -> new PathPackResources(path, resourcePath, false),
                event.getPackType(),
                Pack.Position.TOP,
                PackSource.FEATURE);
        if(pack != null) {
            event.addRepositorySource((packConsumer) -> packConsumer.accept(pack));
        } else {
            LOGGER.warning("Unable to find builtin resource pack at " +  resourcePath);
        }
    }


    public static void registerBuiltinResourcePack(ResourcePackHook.BuiltinResourcePack pack) {
        builtinResourcePacks.add(pack);
    }
}
