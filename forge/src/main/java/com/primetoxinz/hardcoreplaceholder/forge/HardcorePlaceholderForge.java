package com.primetoxinz.hardcoreplaceholder.forge;

import com.primetoxinz.common.resources.forge.ResourcePackHookImpl;
import com.primetoxinz.hardcoreplaceholder.Client;
import dev.architectury.platform.forge.EventBuses;
import com.primetoxinz.hardcoreplaceholder.Common;
import net.minecraft.server.packs.resources.Resource;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(Common.MOD_ID)
public class HardcorePlaceholderForge {
    public HardcorePlaceholderForge() {
		// Submit our event bus to let architectury register our content on the right time
        IEventBus MOD_BUS = FMLJavaModLoadingContext.get().getModEventBus();
        EventBuses.registerModEventBus(Common.MOD_ID, MOD_BUS);
        Common.init();
        MOD_BUS.addListener(this::clientSetup);
        MOD_BUS.addListener(ResourcePackHookImpl::addPackFinders);
        MinecraftForge.EVENT_BUS.register(this);

    }

    private void clientSetup(final FMLClientSetupEvent event) {
        event.enqueueWork(Client::init);
    }
}